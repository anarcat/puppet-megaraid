# install the MegaCLI poprietary RAID monitoring tools
class megaraid(
  Optional[Boolean] $ensure = $::has_megaraid_mod and $::facts['os']['architecture'] == 'amd64',
) {
  if ($ensure) {
    # ensure that we have the non-free repository for the megacli
    # package
    include torproject_org::apt::restricted
    package { 'megacli':
      ensure => installed,
    }
    file { '/usr/local/sbin/megacli':
      ensure => 'link',
      target => '/opt/MegaRAID/MegaCli/MegaCli64',
    }
    file { '/etc/sudoers.d/nagios-rmegacli':
      mode    => '0440',
      content => @("EOF"),
      # FILE MANAGED BY PUPPET, LOCAL CHANGES WILL BE LOST
      nagios          ALL=(ALL)  NOPASSWD: /usr/local/sbin/megacli -adpCount -NoLog
      nagios          ALL=(ALL)  NOPASSWD: /usr/local/sbin/megacli -PdList -a[0-9] -NoLog
      nagios          ALL=(ALL)  NOPASSWD: /usr/local/sbin/megacli -LdGetNum -a[0-9] -NoLog
      nagios          ALL=(ALL)  NOPASSWD: /usr/local/sbin/megacli -LdInfo -L[0-9] -a[0-9] -NoLog
      | EOF
    }
  } else {
    # no need to reverse the apt::restricted repository in this case
    # because the apt module automatically cleans up unmanaged
    # sources.list.d files
    package { 'megacli':
      ensure => purged,
    }
    file { '/usr/local/sbin/megacli':
      ensure => 'absent',
    }
    file { '/etc/sudoers.d/nagios-rmegacli':
      ensure => 'absent',
    }
  }
}
