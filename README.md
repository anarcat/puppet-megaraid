# Puppet MegaRAID module

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-generate-toc again -->
**Table of Contents**

 - [Overview](#overview)
 - [Description](#description)
 - [Usage](#usage)
     - [Hiera data bindings](#hiera-data-bindings)
     - [Facts](#facts)
 - [Limitations](#limitations)
 - [Versioning](#versioning)
 - [Support](#support)
 - [See also](#see-also)

<!-- markdown-toc end -->

## Overview

Manages the MegaCLI packages.

## Description

This module installs the `MegaCLI` packages depending on the
environment (or Hiera parameters).

The only configuration done is a `sudoers.d` snippet for Nagios to be
able to run `MegaCLI` probes.

## Usage

This should install `MegaCLI` depending on your configuration:

    include megaraid

### Hiera data bindings

Alternatively, you can disable each through Hiera:

    ----
    megaraid::ensure: false

### Facts

#### `has_megaraid_mod`

Whether the `megaraid_sas` module is loaded. Can be used to detect a
RAID controller. Note that this will yield false positives if the
module is statically compiled in the kernel.

## Limitations

Performs little or no configuration.

Assumes a `megacli` package is available through the Puppet `Package`
resource. That package could also be installed with the
[jhoblitt/puppet-megaraid][] module instead, but the two modules are
currently not compatible in that sense. This is the only bit of
Tor-specific code, the `torproject_org::apt::restricted` class
inclusion.

## Versioning

This module is versioned according to the [Semantic Versioning
2.0.0][] specification.

[Semantic Versioning 2.0.0]: http://semver.org/spec/v2.0.0.html

## Support

No support channel yet.

## See also

Similar modules:

 * [gardouille/megacli][]: only configures an apt repo and install
   the package, no fact

 * [jhoblitt/puppet-megaraid][]: ships a [`has_megaraid` fact][] which
   relies on `lspci` to find controls, and installs the binaries
   directly from upstream

 * [m4ce/megaraid][]: ships a [`megaraid` fact][] that also uses
   `lspci` to find controllers (but in a looser way), actually
   configures the RAID array (with `StorCLI`) instead of focusing on
   monitoring

 * [lelutin/smartd][]: manages `smartd`, ships many more
   MegaCLI-related facts, but doesn't configure MegaCLI itself

[gardouille/megacli]: https://forge.puppet.com/modules/gardouille/megacli
[`has_megaraid` fact]: https://github.com/jhoblitt/puppet-megaraid/blob/master/lib/facter/has_megaraid.rb_
[jhoblitt/puppet-megaraid]: https://forge.puppet.com/modules/jhoblitt/megaraid_sm
[`megaraid` fact]: https://github.com/m4ce/puppet-megaraid/blob/master/lib/facter/megaraid.rb
[m4ce/megaraid]: https://forge.puppet.com/modules/m4ce/megaraid
[lelutin/smartd]: https://forge.puppet.com/modules/LeLutin/smartd
