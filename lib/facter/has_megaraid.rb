Facter.add("has_megaraid_mod") do
	confine :kernel => :linux
	setcode do
		if FileTest.exist?("/sys/module/megaraid_sas/")
			true
		else
			false
		end
	end
end
